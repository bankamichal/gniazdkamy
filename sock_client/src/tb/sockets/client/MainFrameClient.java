package tb.sockets.client;

import tb.sockets.client.gui.KKButton;
import tb.sockets.client.gui.OrderPane;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

public class MainFrameClient extends JFrame{
	private JPanel contentPane;
	public static OrderPane panel = new OrderPane();
	public static JButton btn1_1,btn1_2,btn1_3,btn2_1,btn2_2,btn2_3,btn3_1,btn3_2,btn3_3, btnExit;
	public static JLabel lblNotConnected = new JLabel("Not Connected", SwingConstants.CENTER);
	public static JLabel lblMove = new JLabel("", SwingConstants.CENTER);
	public static JLabel lblWinner = new JLabel("", SwingConstants.CENTER);
	public static JLabel lblNextGame, lblScore;

	/**
	 * Create the frame.
	 */
	public MainFrameClient() {
		setTitle("CLIENT");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(50, 50, 850, 700);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//PANEL LEFT
		JPanel panelLeft = new JPanel();
		panelLeft.setBounds(10, 10, 200, 600);
		panelLeft.setBackground(Color.LIGHT_GRAY);
		panelLeft.setLayout(null);
		contentPane.add(panelLeft);

		//BUTTON EXIT
		btnExit = new JButton("Disconnect and exit");
		btnExit.setBackground(new Color(0,0,45));
		btnExit.setForeground(Color.WHITE);
		btnExit.setBounds(10,560,180,30);
		panelLeft.add(btnExit);
		btnExit.addActionListener(e -> {
			Data.finish = true;
		});

		//LABEL CONNECTED
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBackground(Color.RED);
		lblNotConnected.setForeground(Color.WHITE);
		lblNotConnected.setBorder(new LineBorder(Color.BLACK,2,true));
		lblNotConnected.setBounds(10, 1, 180, 30);
		panelLeft.add(lblNotConnected);

		//LABEL MOVE
		lblMove.setBounds(10,260,180,30);
		panelLeft.add(lblMove);

		//PANEL BUTTONS
		JPanel panelButtons = new OrderPane();
		panelButtons.setLayout(new GridLayout(3, 3, 3, 3));
		panelButtons.setBounds(25,300,155,155);
		panelButtons.setBorder(new LineBorder(Color.BLACK,3,true));
		panelLeft.add(panelButtons);

		//LABEL WINNER
		lblWinner.setBounds(0,100,200,30);
		lblWinner.setVisible(false);
		lblWinner.setOpaque(true);
		lblWinner.setBackground(Color.ORANGE);
		lblWinner.setBorder(new LineBorder(Color.BLACK,2,true));
		panelLeft.add(lblWinner);

		//LABEL NEXTGAME
		lblNextGame= new JLabel("", SwingConstants.CENTER);
		lblNextGame.setVisible(false);
		lblNextGame.setBounds(10,150,180,30);
		lblNextGame.setBackground(Color.darkGray);
		lblNextGame.setForeground(Color.WHITE);
		lblNextGame.setOpaque(true);
		panelLeft.add(lblNextGame);

		//LABEL SCORE
		lblScore = new JLabel("You 0 : 0 Opponent",SwingConstants.CENTER);
		lblScore.setBackground(Color.DARK_GRAY);
		lblScore.setForeground(Color.WHITE);
		lblScore.setBounds(10,200,180,30);
		lblScore.setOpaque(true);
		panelLeft.add(lblScore);

		//PRAWY PANEL
		panel.setBounds(220,10,600,600);
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);

		btn1_1 = new KKButton();
		btn1_1.addActionListener(e -> {
			if (!Data.turn)
			{
				btn1_1.setEnabled(false);
				Data.nextMove = 0;
			}
		});
		panelButtons.add(btn1_1);

		btn1_2 = new KKButton();
		btn1_2.addActionListener(e -> {
			if (!Data.turn)
			{
				btn1_2.setEnabled(false);
				Data.nextMove = 1;
			}
		});
		panelButtons.add(btn1_2);

		btn1_3 = new KKButton();
		btn1_3.addActionListener(e -> {
			if (!Data.turn)
			{
				btn1_3.setEnabled(false);
				Data.nextMove = 2;
			}
		});
		panelButtons.add(btn1_3);

		btn2_1 = new KKButton();
		btn2_1.addActionListener(e -> {
			if (!Data.turn)
			{
				btn2_1.setEnabled(false);
				Data.nextMove = 3;
			}
		});
		panelButtons.add(btn2_1);

		btn2_2 = new KKButton();
		btn2_2.addActionListener(e -> {
			if (!Data.turn)
			{
				btn2_2.setEnabled(false);
				Data.nextMove = 4;
			}
		});
		panelButtons.add(btn2_2);

		btn2_3 = new KKButton();
		btn2_3.addActionListener(e -> {
			if (!Data.turn)
			{
				btn2_3.setEnabled(false);
				Data.nextMove = 5;
			}
		});
		panelButtons.add(btn2_3);

		btn3_1 = new KKButton();
		btn3_1.addActionListener(e -> {
			if (!Data.turn)
			{
				btn3_1.setEnabled(false);
				Data.nextMove = 6;
			}
		});
		panelButtons.add(btn3_1);

		btn3_2 = new KKButton();
		btn3_2.addActionListener(e -> {
			if (!Data.turn)
			{
				btn3_2.setEnabled(false);
				Data.nextMove = 7;
			}
		});
		panelButtons.add(btn3_2);

		btn3_3 = new KKButton();
		btn3_3.addActionListener(e -> {
			if (!Data.turn)
			{
				btn3_3.setEnabled(false);
				Data.nextMove = 8;
			}
		});
		panelButtons.add(btn3_3);


	}

	public static void operateOpponentMove(int nextMove)
	{
		switch(nextMove)
		{
			case 0:
				btn1_1.setEnabled(false);
				panel.drawShape(true,0);
				Data.board[0][0] = 1;
				break;
			case 1:
				btn1_2.setEnabled(false);
				panel.drawShape(true,1);
				Data.board[0][1] = 1;
				break;
			case 2:
				btn1_3.setEnabled(false);
				panel.drawShape(true,2);
				Data.board[0][2] = 1;
				break;
			case 3:
				btn2_1.setEnabled(false);
				panel.drawShape(true,3);
				Data.board[1][0] = 1;
				break;
			case 4:
				btn2_2.setEnabled(false);
				panel.drawShape(true,4);
				Data.board[1][1] = 1;
				break;
			case 5:
				btn2_3.setEnabled(false);
				panel.drawShape(true,5);
				Data.board[1][2] = 1;
				break;
			case 6:
				btn3_1.setEnabled(false);
				panel.drawShape(true,6);
				Data.board[2][0] = 1;
				break;
			case 7:
				btn3_2.setEnabled(false);
				panel.drawShape(true,7);
				Data.board[2][1] = 1;
				break;
			case 8:
				btn3_3.setEnabled(false);
				panel.drawShape(true,8);
				Data.board[2][2] = 1;
				break;
			case 10:
				btn1_1.setEnabled(true);
				btn1_2.setEnabled(true);
				btn1_3.setEnabled(true);
				btn2_1.setEnabled(true);
				btn2_2.setEnabled(true);
				btn2_3.setEnabled(true);
				btn3_1.setEnabled(true);
				btn3_2.setEnabled(true);
				btn3_3.setEnabled(true);

				Data.finish = false;
				Data.nextMove = 10;
				Data.lastMove = 10;
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						Data.board[i][j] = -1;
					}
				}
				Data.turn = false;
				Data.moves = 0;
				panel.clearGrid();
				lblNextGame.setVisible(true);
		}
	}
	public static boolean checkIfFinish()
	{
		if (Data.moves >= 5)
		{
			//SPRAWDZANIE POZIOMYCH TROJEK
			//dla clienta = 0 i servera = 1
			//dla kazdej linii
			for (int j = 0; j <= 2; j++) {
				if (Data.board[j][0]==Data.board[j][1] && Data.board[j][1]==Data.board[j][2] && Data.board[j][0]!= -1) {
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(50, 100 + (200 * j), 550, 100 + (200 * j));
					lblMove.setText("The end.");
					lblWinner.setVisible(true);
					if (Data.board[j][0] == 0) {
						lblWinner.setText("YOU WON!");
						Data.clientWon++;
					}
					else {
						lblWinner.setText("YOU LOST :(");
						Data.serverWon++;
					}
					return true;
				}
			}
			//SPRAWDZANIE PIONOWO
			//dla clienta = 0 i servera = 1
			//dla kazdej linii
			for (int j = 0; j <= 2; j++) {

				if (Data.board[0][j]==Data.board[1][j] && Data.board[1][j]==Data.board[2][j] && Data.board[0][j]!= -1) {
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(100 + (200 * j), 50, 100 + (200 * j), 550);
					lblWinner.setVisible(true);
					lblMove.setText("The end.");

					if (Data.board[0][j] == 0) {
						lblWinner.setText("YOU WON!");
						Data.clientWon++;
					}
					else {
						lblWinner.setText("YOU LOST :(");
						Data.serverWon++;
					}
					return true;
				}

			}
			//SPRAWDZANIE NA UKOS \
			for (int i = 0; i <= 1; i++)
			{
				if (Data.board[0][0]==Data.board[1][1] && Data.board[1][1]==Data.board[2][2] && Data.board[0][0]!=-1)
				{
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(50,50,550,550);
					lblWinner.setVisible(true);
					lblMove.setText("The end.");

					if (i == 0)
					{
						lblWinner.setText("YOU WON!");
						Data.clientWon++;
					}
					else
					{
						lblWinner.setText("YOU LOST :(");
						Data.serverWon++;
					}
					return true;
				}
			}
			//SPRAWDZANIE NA UKOS /
			for (int i = 0; i <= 1; i++)
			{
				if (Data.board[0][2]==Data.board[1][1] && Data.board[2][0]==Data.board[1][1] && Data.board[0][2]!=-1)
				{
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(550,50,50,550);
					lblWinner.setVisible(true);
					lblMove.setText("The end.");
					if (i == 0)
					{
						lblWinner.setText("YOU WON!");
						Data.clientWon++;
					}
					else
					{
						lblWinner.setText("YOU LOST :(");
						Data.serverWon++;
					}
					return true;
				}
			}
			if (Data.moves == 9)
			{
				lblWinner.setText("It's a draw...");
				lblWinner.setVisible(true);
				lblMove.setText("The end.");
				return true;
			}
		}
		return false;
	}

}
