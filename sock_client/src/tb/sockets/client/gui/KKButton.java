
package tb.sockets.client.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author tb
 *
 */
@SuppressWarnings("serial")
public class KKButton extends JButton {

	private BufferedImage[] bufferedImages = new BufferedImage[9];
	private int stan = 0;

	public KKButton() {
		super("");
		for (int i=0;i<3; i++) {
			for (int j = 0; j < 3; j++) {
				BufferedImage image = new BufferedImage(50, 50, BufferedImage.TYPE_INT_RGB);
				Graphics2D graphics = (Graphics2D) image.getGraphics();
				graphics.setColor(Color.WHITE);
				graphics.fillRect(0, 0,50, 50);
				graphics.setColor(Color.BLACK);
				graphics.drawOval(10,10,30,30);
				bufferedImages[i] = image;
			}
		}
	}
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.drawImage(bufferedImages[0], 0, 0, null);
	}
}
