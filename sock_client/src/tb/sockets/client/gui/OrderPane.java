package tb.sockets.client.gui;

import javax.swing.*;
import java.awt.*;

public class OrderPane extends JPanel {

	/**
	 * Create the panel.
	 */
	public OrderPane() {
		setBackground(new Color(255, 255, 240));
	}

	public void drawShape(boolean wybor, int okno)
	{
		Graphics2D g2 = (Graphics2D) getGraphics();
		g2.setStroke(new BasicStroke(5));
		if(!wybor)
		{
			g2.setColor(Color.BLACK);
			g2.drawOval(200*(okno%3) + 25, 200*(okno/3) + 25, 200 - 50, 200 - 50);
		}
		else
		{
			g2.setColor(Color.BLACK);
			g2.setColor(Color.BLACK);
			g2.drawLine(200*(okno%3) + 25, (200*(okno/3)) + 25, 200*(okno%3) + 175, (200*(okno/3)) + 175);
			g2.drawLine(200*(okno%3) + 25, (200*(okno/3)) + 175, 200*(okno%3) + 175, (200*(okno/3)) + 25);
		}
	}
	public void clearGrid()
	{
		Graphics2D g = (Graphics2D) getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 600, 600);
		g.setColor(Color.BLACK);
		g.drawLine(200, 0, 200, 600);
		g.drawLine(400, 0, 400, 600);
		g.drawLine(0, 200, 600, 200);
		g.drawLine(0, 400, 600, 400);
		g.drawRect(0, 0, 599, 599);
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 600, 600);
		g.setColor(Color.BLACK);
		g.drawLine(200, 0, 200, 600);
		g.drawLine(400, 0, 400, 600);
		g.drawLine(0, 200, 600, 200);
		g.drawLine(0, 400, 600, 400);
		g.drawRect(0, 0, 599, 599);
	}

}
