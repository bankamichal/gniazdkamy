package tb.sockets.client;

import java.awt.*;

/**
 * Created by MichalPC on 2017-12-20, 00:16.
 */
public class Main {
    private String host;
    private int port;

    public static void main(String[] args) throws InterruptedException {

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainFrameClient frame = new MainFrameClient();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Thread client = new Thread()
        {
            public void run()
            {
                new Client().run();
            }
        };
        client.start();
    }
}
