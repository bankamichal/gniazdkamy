package tb.sockets.client;

import java.awt.*;
import java.io.*;
import java.net.Socket;

/**
 * Created by MichalPC on 2017-12-20, 00:14.
 */
public class Client implements Runnable{
    final int serverPort = 6666;
    private static Socket socket;
    private static BufferedReader fromServer;
    private static PrintWriter toServer;
    private static int received=10;
    private static OutputStream ostream;
    private static InputStream istream;

    private void lblNextRoundText()
    {
        String s = "Next game for ";
        for (int i = 0 ; i <= 5; i++)
        {
            s = "Next game for " + (5-i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MainFrameClient.lblNextGame.setText(s);
        }
    }

    @Override
    public void run() {
        try {
            socket = new Socket("localhost", serverPort);
            fromServer = new BufferedReader(new InputStreamReader(System.in));
            ostream = socket.getOutputStream();
            istream = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        toServer = new PrintWriter(ostream, true);
        fromServer = new BufferedReader(new InputStreamReader(istream));
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Data.board[i][j] = -1;
            }
        }

        if (socket.isConnected())
        {
            MainFrameClient.lblNotConnected.setText("Connected");
            MainFrameClient.lblNotConnected.setOpaque(true);
            MainFrameClient.lblMove.setText("YOUR MOVE");

            while(!Data.finish)
            {
                MainFrameClient.lblNotConnected.setBackground(Color.green);
                MainFrameClient.lblNotConnected.setForeground(Color.black);

                if ( MainFrameClient.checkIfFinish())
                {
                    MainFrameClient.lblScore.setText("You " + Data.clientWon + " : " + Data.serverWon + " Opponent");
                    MainFrameClient.lblNextGame.setVisible(true);
                    MainFrameClient.lblScore.setVisible(true);
                    lblNextRoundText();
                    MainFrameClient.lblNextGame.setVisible(false);
                    Data.finish = false;
                    Data.lastMove = 10;
                    Data.nextMove = 10;
                    Data.turn = false;
                    Data.moves = 0;
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            Data.board[i][j] = -1;
                        }
                    }

                    MainFrameClient.btn1_1.setEnabled(true);
                    MainFrameClient.btn1_2.setEnabled(true);
                    MainFrameClient.btn1_3.setEnabled(true);
                    MainFrameClient.btn2_1.setEnabled(true);
                    MainFrameClient.btn2_2.setEnabled(true);
                    MainFrameClient.btn2_3.setEnabled(true);
                    MainFrameClient.btn3_1.setEnabled(true);
                    MainFrameClient.btn3_2.setEnabled(true);
                    MainFrameClient.btn3_3.setEnabled(true);
                    MainFrameClient.lblWinner.setVisible(false);
                    MainFrameClient.panel.clearGrid();
                }

                if(!Data.turn)
                {
                    if(Data.nextMove!=Data.lastMove)
                    {
                        toServer.write(Data.nextMove);
                        toServer.flush();
                        Data.moves++;
                        Data.lastMove = Data.nextMove;
                        Data.board[Data.lastMove / 3][Data.lastMove % 3] = 0;
                        MainFrameClient.lblMove.setText("OPPONENT'S TURN...");
                        MainFrameClient.panel.drawShape(false,Data.nextMove);
                        System.out.println("SEND TO SERVER= " + Data.nextMove);
                        Data.turn = true;
                    }
                }
                else
                {
                    try {
                        received = istream.read();
                    } catch (IOException e) {
                        MainFrameClient.lblNotConnected.setText("Disconnected");
                        MainFrameClient.lblNotConnected.setBackground(Color.red);
                        e.printStackTrace();
                    }
                    if(received != Data.lastMove) {
                        Data.turn = false;
                        System.out.println("RECIEVED FROM SERVER = " + received);
                        MainFrameClient.operateOpponentMove(received);
                        Data.moves++;
                        Data.lastMove = received;
                        Data.nextMove = received;
                        MainFrameClient.lblMove.setText("YOUR MOVE");
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            MainFrameClient.lblMove.setVisible(false);
            MainFrameClient.lblNotConnected.setText("Disconnected");
            MainFrameClient.lblNotConnected.setBackground(Color.red);
            try {
                socket.close();
                ostream.close();
                toServer.close();
                istream.close();
                fromServer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.exit(0);
    }
}
