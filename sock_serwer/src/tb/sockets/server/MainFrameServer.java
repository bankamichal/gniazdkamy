package tb.sockets.server;

import tb.sockets.server.gui.KKButton;
import tb.sockets.server.gui.OrderPane;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

public class MainFrameServer extends JFrame{

	private JPanel contentPane;
	public static OrderPane panel;
	public static JButton btn1_1,btn1_2,btn1_3,btn2_1,btn2_2,btn2_3,btn3_1,btn3_2,btn3_3, btnExit;
	public static JLabel lblNotConnected, lblWinner = new JLabel("", SwingConstants.CENTER), lblNextGame, lblScore;
	public static  JLabel lblMove = new JLabel("Turn");
	/**
	 * Create the frame.
	 */
	public MainFrameServer() {
		setTitle("SERVER");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(1000, 100, 850, 700);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//PANEL LEFT
		JPanel panelLeft = new JPanel();
		panelLeft.setBounds(10, 10, 200, 600);
		panelLeft.setBackground(Color.LIGHT_GRAY);
		panelLeft.setLayout(null);
		contentPane.add(panelLeft);

		//BUTTON EXIT
		btnExit = new JButton("Disconnect and exit");
		btnExit.setBackground(new Color(0,0,45));
		btnExit.setForeground(Color.WHITE);
		btnExit.setBounds(10,560,180,30);
		panelLeft.add(btnExit);
		btnExit.addActionListener(e -> {
			System.exit(0);
		});

		//LABEL CONNECTED
		lblNotConnected = new JLabel("Not Connected", SwingConstants.CENTER);
		lblNotConnected.setBackground(Color.red);
		lblNotConnected.setForeground(Color.WHITE);
		lblNotConnected.setBorder(new LineBorder(Color.BLACK,2,true));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 1, 180, 30);
		panelLeft.add(lblNotConnected);

		//LABEL MOVE
		lblMove = new JLabel("",SwingConstants.CENTER);
		lblMove.setText("OPPONENT'S MOVE...");
		lblMove.setBounds(10,260,180,30);
		panelLeft.add(lblMove);

		//PANEL BUTTONS
		JPanel panelButtons = new OrderPane();
		panelButtons.setLayout(new GridLayout(3, 3, 3, 3));
		panelButtons.setBounds(25,300,155,155);
		panelButtons.setBorder(new LineBorder(Color.BLACK,3,true));
		panelLeft.add(panelButtons);

		//LABEL WINNER
		lblWinner.setVisible(false);
		lblWinner.setBounds(0,100,200,30);
		lblWinner.setBorder(new LineBorder(Color.BLACK,2,true));
		lblWinner.setOpaque(true);
		lblWinner.setBackground(Color.orange);
		panelLeft.add(lblWinner);

		//LABEL NEXTGAME
		lblNextGame= new JLabel("", SwingConstants.CENTER);
		lblNextGame.setVisible(false);
		lblNextGame.setBounds(10,150,180,30);
		lblNextGame.setBackground(Color.darkGray);
		lblNextGame.setForeground(Color.WHITE);
		lblNextGame.setOpaque(true);
		panelLeft.add(lblNextGame);

		//LABEL SCORE
		lblScore = new JLabel("You 0 : 0 Opponent",SwingConstants.CENTER);
		lblScore.setBackground(Color.DARK_GRAY);
		lblScore.setForeground(Color.WHITE);
		lblScore.setBounds(10,200,180,30);
		lblScore.setOpaque(true);
		panelLeft.add(lblScore);

		//PRAWY PANEL
		panel = new OrderPane();
		panel.setBounds(220,10,600,600);
		panel.setBackground(Color.WHITE);
		contentPane.add(panel);

		btn1_1 = new KKButton();
		btn1_1.addActionListener(e -> {
			if (Data.turn)
			{
				Data.nextMove = 0;
				btn1_1.setEnabled(false);
			}
		});
		panelButtons.add(btn1_1);

		btn1_2 = new KKButton();
		btn1_2.addActionListener(e -> {
			if (Data.turn) {
				Data.nextMove = 1;
				btn1_2.setEnabled(false);
			}
		});
		panelButtons.add(btn1_2);

		btn1_3 = new KKButton();
		btn1_3.addActionListener(e -> {
			if (Data.turn) {
				Data.nextMove = 2;
				btn1_3.setEnabled(false);
			}
		});
		panelButtons.add(btn1_3);

		btn2_1 = new KKButton();
		btn2_1.addActionListener(e -> {
			if(Data.turn)
			{
				Data.nextMove = 3;
				btn2_1.setEnabled(false);
			}
			});
		panelButtons.add(btn2_1);

		btn2_2 = new KKButton();
		btn2_2.addActionListener(e -> {
			if (Data.turn) {
				Data.nextMove = 4;
				btn2_2.setEnabled(false);
			}
		});
		panelButtons.add(btn2_2);

		btn2_3 = new KKButton();
		btn2_3.addActionListener(e -> {
			if(Data.turn){
			Data.nextMove = 5;
			btn2_3.setEnabled(false);
			}
		});
		panelButtons.add(btn2_3);

		btn3_1 = new KKButton();
		btn3_1.addActionListener(e -> {
			if (Data.turn)
			{
				Data.nextMove = 6;
				btn3_1.setEnabled(false);
			}
		});
		panelButtons.add(btn3_1);

		btn3_2 = new KKButton();
		btn3_2.addActionListener(e -> {
			if (Data.turn) {
				Data.nextMove = 7;
				btn3_2.setEnabled(false);
			}
		});
		panelButtons.add(btn3_2);

		btn3_3 = new KKButton();
		btn3_3.addActionListener(e -> {
			if (Data.turn) {
				Data.nextMove = 8;
				btn3_3.setEnabled(false);
			}
		});
		panelButtons.add(btn3_3);

	}
	public static void operateOpponentMove(int nextMove)
	{
		switch(nextMove)
		{
			case 0:
				btn1_1.setEnabled(false);
				panel.drawShape(false,0);
				Data.board[0][0] = 0;
				break;
			case 1:
				btn1_2.setEnabled(false);
				panel.drawShape(false,1);
				Data.board[0][1] = 0;
				break;
			case 2:
				btn1_3.setEnabled(false);
				panel.drawShape(false,2);
				Data.board[0][2] = 0;
				break;
			case 3:
				btn2_1.setEnabled(false);
				panel.drawShape(false,3);
				Data.board[1][0] = 0;
				break;
			case 4:
				btn2_2.setEnabled(false);
				panel.drawShape(false,4);
				Data.board[1][1] = 0;
				break;
			case 5:
				btn2_3.setEnabled(false);
				panel.drawShape(false,5);
				Data.board[1][2] = 0;
				break;
			case 6:
				btn3_1.setEnabled(false);
				panel.drawShape(false,6);
				Data.board[2][0] = 0;
				break;
			case 7:
				btn3_2.setEnabled(false);
				panel.drawShape(false,7);
				Data.board[2][1] = 0;
				break;
			case 8:
				btn3_3.setEnabled(false);
				panel.drawShape(false,8);
				Data.board[2][2] = 0;
				break;
			case 10:
				btn1_1.setEnabled(true);
				btn1_2.setEnabled(true);
				btn1_3.setEnabled(true);
				btn2_1.setEnabled(true);
				btn2_2.setEnabled(true);
				btn2_3.setEnabled(true);
				btn3_1.setEnabled(true);
				btn3_2.setEnabled(true);
				btn3_3.setEnabled(true);

				Data.finish = false;
				Data.nextMove = 10;
				Data.lastMove = 10;
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						Data.board[i][j] = -1;
					}
				}
				Data.turn = false;
				Data.moves = 0;
				panel.clearGrid();
				lblNextGame.setVisible(true);
		}
	}

	public static boolean checkIfFinish()
	{
		if (Data.moves >= 3)
		{
			//SPRAWDZANIE POZIOMYCH TROJEK
			//dla kazdej linii
			for (int j = 0; j <= 2; j++) {
				if (Data.board[j][0]==Data.board[j][1] && Data.board[j][1]==Data.board[j][2] && Data.board[j][0]!= -1) {
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(50, 100 + (200 * j), 550, 100 + (200 * j));
					lblWinner.setVisible(true);
					lblMove.setText("The end.");
					lblNextGame.setVisible(true);
					if (Data.board[j][0] == 0) { lblWinner.setText("You lost :(");}
					else {lblWinner.setText("YOU WON!");}
					return true;
				}
			}
			//SPRAWDZANIE PIONOWO
			//dla kazdej linii
			for (int j = 0; j <= 2; j++) {
				if (Data.board[0][j]==Data.board[1][j] && Data.board[1][j]==Data.board[2][j] && Data.board[0][j]!= -1) {
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(100 + (200 * j), 50, 100 + (200 * j), 550);
					lblWinner.setVisible(true);
					lblMove.setText("The end.");
					lblNextGame.setVisible(true);

					if (Data.board[0][j] == 0) {
						lblWinner.setText("You lost :(");
						Data.clientWon++;
					}
					else {
						lblWinner.setText("YOU WON!");
						Data.serverWon++;
					}
					return true;
				}

			}
			//SPRAWDZANIE NA UKOS \
			for (int i = 0; i <= 1; i++)
			{
				if (Data.board[0][0]==Data.board[1][1] && Data.board[1][1]==Data.board[2][2] && Data.board[0][0]!=-1)
				{
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(50,50,550,550);
					lblWinner.setVisible(true);
					lblMove.setText("The end.");
					lblNextGame.setVisible(true);
					if (i == 0)
					{
						lblWinner.setText("You lost :(");
						Data.clientWon++;
					}
					else
					{
						lblWinner.setText("YOU WON!");
						Data.serverWon++;
					}
					return true;
				}
			}
			//SPRAWDZANIE NA UKOS /
			for (int i = 0; i <= 1; i++)
			{
				if (Data.board[0][2]==Data.board[1][1] && Data.board[2][0]==Data.board[1][1] && Data.board[0][2]!=-1)
				{
					Graphics2D g2 = (Graphics2D) panel.getGraphics();
					g2.setStroke(new BasicStroke(5));
					g2.setColor(Color.RED);
					g2.drawLine(550,50,50,550);
					lblWinner.setVisible(true);
					lblMove.setText("The end.");
					lblNextGame.setVisible(true);
					if (i == 0)
					{
						lblWinner.setText("You lost :(");
						Data.clientWon++;
					}
					else
					{
						lblWinner.setText("YOU WON!");
						Data.serverWon++;
					}
					return true;
				}
			}
			if (Data.moves == 9)
			{
				System.out.println("REMIS");
				lblWinner.setText("It's a draw...");
				lblWinner.setVisible(true);
				lblMove.setText("The end.");
				return true;
			}
		}
		return false;
	}
}
