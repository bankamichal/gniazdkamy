package tb.sockets.server;

/**
 * Created by MichalPC on 2017-12-20, 02:01.
 */
public final class Data {
    public static volatile int lastMove = 10;
    public static volatile int nextMove = 10;
    public static volatile int[][] board = new int[3][3];
    public static volatile boolean turn = false;
    public static volatile int moves = 0;
    public static volatile boolean finish = false;
    public static volatile int serverWon = 0;
    public static volatile int clientWon = 0;


}
