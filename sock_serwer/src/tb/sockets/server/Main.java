package tb.sockets.server;

import java.awt.*;

/**
 * Created by MichalPC on 2017-12-20, 00:12.
 */
public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainFrameServer frame = new MainFrameServer();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Thread server = new Thread()
        {
            public void run()
            {
                Server s = new Server();
                s.run();
            }
        };
        server.start();

    }
}
