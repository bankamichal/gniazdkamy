package tb.sockets.server;

import java.awt.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by MichalPC on 2017-12-20, 00:11.
 */
public class Server implements Runnable {
    private final int serverPort = 6666;
    private ServerSocket serverSocket = null;
    private Socket server = null;

    private static int received = 10;
    private OutputStream ostream;
    private PrintWriter toClient;
    private InputStream istream;
    private BufferedReader fromClient;

    private void lblNextRoundText()
    {
        String s = "";
        for (int i = 0 ; i <= 5; i++)
        {
            s = "Next game for " + (5-i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MainFrameServer.lblNextGame.setText(s);
        }
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(serverPort);
            server = serverSocket.accept();
            ostream = server.getOutputStream();
            toClient = new PrintWriter(ostream, true);
            istream = server.getInputStream();
            fromClient = new BufferedReader(new InputStreamReader(istream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Data.board[i][j] = -1;
            }
        }

        if (server.isConnected()) {
            MainFrameServer.lblNotConnected.setText("Connected");
            MainFrameServer.lblNotConnected.setBackground(Color.green);
            MainFrameServer.lblNotConnected.setForeground(Color.BLACK);

            while (!Data.finish) {
                toClient.flush();
                if ( MainFrameServer.checkIfFinish())
                {
                    MainFrameServer.lblScore.setText("You " + Data.serverWon + " : " + Data.clientWon + " Opponent");
                    MainFrameServer.lblNextGame.setVisible(true);
                    MainFrameServer.lblWinner.setVisible(true);
                    lblNextRoundText();
                    MainFrameServer.lblWinner.setVisible(false);
                    MainFrameServer.lblNextGame.setVisible(false);
                    Data.finish = false;
                    Data.turn = false;
                    Data.lastMove = 10;
                    Data.nextMove = 10;
                    Data.moves = 0;
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            Data.board[i][j] = -1;
                        }
                    }

                    MainFrameServer.btn1_1.setEnabled(true);
                    MainFrameServer.btn1_2.setEnabled(true);
                    MainFrameServer.btn1_3.setEnabled(true);
                    MainFrameServer.btn2_1.setEnabled(true);
                    MainFrameServer.btn2_2.setEnabled(true);
                    MainFrameServer.btn2_3.setEnabled(true);
                    MainFrameServer.btn3_1.setEnabled(true);
                    MainFrameServer.btn3_2.setEnabled(true);
                    MainFrameServer.btn3_3.setEnabled(true);

                    MainFrameServer.panel.clearGrid();
                }
                if (Data.turn) {

                    if (Data.nextMove != Data.lastMove) {
                        MainFrameServer.panel.drawShape(true, Data.nextMove);
                        toClient.write(Data.nextMove);
                        Data.moves++;
                        Data.lastMove = Data.nextMove;
                        Data.board[Data.lastMove / 3][Data.lastMove % 3] = 1;
                        Data.turn = false;
                        toClient.flush();
                        MainFrameServer.lblMove.setText("OPPONENT'S MOVE...");
                    }
                }
                else
                {
                    try {
                        received = fromClient.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (received != Data.lastMove) {
                        System.out.println("RECIEVED FROM CLIENT= " + received);
                        Data.lastMove = received;
                        Data.nextMove = received;
                        MainFrameServer.operateOpponentMove(received);
                        Data.moves++;
                        Data.turn = true;
                        MainFrameServer.lblMove.setText("YOUR MOVE");
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            MainFrameServer.lblNotConnected.setText("Disconnected");
            MainFrameServer.lblNotConnected.setBackground(Color.red);
            try {
                fromClient.close();
                istream.close();
                toClient.close();
                ostream.close();
                server.close();
               serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.exit(0);
    }
}
